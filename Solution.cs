﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace PermMissingElem
{
    class Solution
    {
        public int solution(int[] A)
        {
            var N = Enumerable.Range(1, A.Length + 1);
            int missingNumber = N.Except(A).FirstOrDefault();
            return missingNumber;
        }
    }
}
